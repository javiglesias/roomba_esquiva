package code;
import java.io.*;
import roombacomm.*;

/**
 *  Read sensors to detect bumps and turn away from them while driving
 * <p>
 *  Run it with something like: <pre>
 *   java roombacomm.BumpTurn /dev/cu.KeySerial1
 *  </pre>
 *
 */
public class Roomba_j_d_j_v1 extends Thread implements Runnable
{
	public static final int IR_left = 52;
	public static final int IR_right = 53;
	public static Roomba_j_d_j_v1 th = new Roomba_j_d_j_v1();
	public static RoombaCommSerial roombacomm = new RoombaCommSerial();
	//public static Move_Thread move = new Move_Thread(roombacomm);
    static String usage = 
        "Uso: \n"+
        "  ejroomba puertoserie\n" +
        "\n";
    static boolean debug = false;
    static boolean hwhandshake = false;
    public Roomba_j_d_j_v1() 
    {
	}
    public static void main(String[] args) {
        if( args.length < 1 ) {
            System.out.println( usage );
            System.exit(0);
        }

        String portname = args[0];  // e.g. "/dev/cu.KeySerial1"
        roombacomm.setProtocol("SCI"); //Si es un modelo antiguo     
        //roombacomm.setProtocol("OI"); //Si es un modelo nuevo 5XX negro
        roombacomm.debug = debug;
        roombacomm.waitForDSR = hwhandshake;
        System.out.println("Roomba startup");
        if( ! roombacomm.connect( portname ) ) {
            System.out.println("Couldn't connect to "+portname);
            System.exit(1);
        }
        // Inicializar el roomba en modo seguro
        roombacomm.startup();
        roombacomm.control();
        // Poner el roomba en modo control total
        while(!keyIsPressed())
        {
        	roombacomm.setSensorsAutoUpdate(true);
        	roombacomm.startAutoUpdate();
        	roombacomm.setSpeed(1000);
        	if(roombacomm.bumpRight())
        	{
		        while(roombacomm.bumpRight())
		        {
		        	System.out.println("Bumper Right Pulsado. CUIDADO RADIACTIVO_MAN");
		        	System.out.println("MOVIENDOME HACIA LA IZQ");
		        	roombacomm.spinLeft(60);
		        	//roombacomm.updateSensors();
		        }
        	}
        	//roombacomm.updateSensors();
        	if(roombacomm.bumpLeft())
        	{
		        while(roombacomm.bumpLeft())
		        {
		        	System.out.println("Bumper Left Pulsado. CUIDADO RADIACTIVO_MAN");
		        	System.out.println("MOVIENDOME HACIA LA DER");
		        	roombacomm.spinRight(60);
		        	//roombacomm.updateSensors();
		        }
        	}
        	//roombacomm.updateSensors();
        	if(roombacomm.bumpRight() && roombacomm.bumpRight())
        	{
		        while(roombacomm.bumpLeft() && roombacomm.bumpLeft())
		        {
		        	System.out.println("DE FRENTE ZOQUETE!!!!, DA LA VUELTA, NENE");
		        	roombacomm.spinRight(180);
		        	//roombacomm.updateSensors();
		        }
        	}
        	else
        		while(!roombacomm.bumpLeft() && !roombacomm.bumpLeft())
    	        {
            		//roombacomm.updateSensors();
            		roombacomm.goForward(300);
            		if(roombacomm.bumpRight())
            			break;
            		if(roombacomm.bumpLeft())
            			break;
            		//roombacomm.updateSensors();
    	        }
        }
        roombacomm.stop();
        System.out.println("Disconnecting");
        roombacomm.disconnect();
        System.out.println("Done");
    }

    /** check for keypress, return true if so */
    public static boolean keyIsPressed() {
        boolean press = false;
        try { 
            if( System.in.available() != 0 ) {
                System.out.println("key pressed");
                press = true;
            }
        } catch( IOException ioe ) { }
        return press;
    }
}

